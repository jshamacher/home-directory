# Sourced on all invocations of the shell unless the -f option is set.
# Should contain commands to set the command search path and other important
# environment variables.
# Should not contain commands that produce output or assume that there is a TTY.

# Ensure path arrays do not contain duplicates.
typeset -gU path

# Having this wrapped in the test is a workaround for an undesirable, but
# apparently unavoidable, interaction between tmux and RVM.
# The -gU setup above ensures uniqueness but does not prevent ordering changes.
# In a shell outside of tmux, path is set up here, then the RVM directories are
# added in .zshrc and everything works splendidly.
# But when tmux starts up, path already contains the RVM stuff.  This reorders
# the entries so that my directories are first.  RVM doesn't try to re-add its
# paths, so they stay later in the list.
# That results in a warning from RVM about the order of path entries.
# It's really kind of annoying, and I think the warning is basically harmless,
# but it still offends my sense of order to see that warning.
if [[ ! :$PATH: == *:"$HOME/bin":* ]] ; then
    path=(
        $HOME/bin
        /usr/local/{bin,sbin}
        /snap/bin
        $path
    )
fi
