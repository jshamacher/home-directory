[[ -s "$HOME/.profile" ]] && source "$HOME/.profile" # Load the default .profile

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

export PATH=$PATH:/opt/PostgreSQL/8.4/bin:/usr/local/bin

export PAGER="less -r"

export TERM=xterm-color
test -r /sw/bin/init.sh && . /sw/bin/init.sh
