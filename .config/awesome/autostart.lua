local awful = require("awful")

-- {{{ Network Manager - see http://awesome.naquadah.org/wiki/Nm-applet
awful.util.spawn_with_shell("/usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1")
awful.util.spawn_with_shell("/home/hamacher/bin/run_once.sh nm-applet")
-- }}}

-- Always want a Firefox instance.
awful.util.spawn_with_shell("/home/hamacher/bin/run_once.sh firefox")

-- And a terminal.
awful.util.spawn_with_shell("/home/hamacher/bin/run_once.sh konsole")
