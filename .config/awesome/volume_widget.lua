-- Based on https://awesome.naquadah.org/wiki/Volume_control_and_display
-- "For a numeric volume display" section.

local awful = require("awful")
local wibox = require("wibox")

local volume_widget = wibox.widget.textbox()

function update_volume(widget)
   local fd = io.popen("amixer sget Master")
   local status = fd:read("*all")
   fd:close()

   local volume = string.match(status, "(%d?%d?%d)%%")
   volume = string.format("% 3d", volume)

   status = string.match(status, "%[(o[^%]]*)%]")

   if string.find(status, "on", 1, true) then
       -- For the volume numbers
       volume = "Volume: " .. volume .. "%"
   else
       -- For the mute button
       volume = "Volume: " .. volume .. "M"
   end
   widget:set_markup(volume)
end

update_volume(volume_widget)

mytimer = timer({ timeout = 0.2 })
mytimer:connect_signal("timeout", function () update_volume(volume_widget) end)
mytimer:start()

return volume_widget
