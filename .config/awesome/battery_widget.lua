-- Based on http://awesome.naquadah.org/wiki/Gigamo_Battery_Widget

local beautiful = require("beautiful")
local naughty   = require("naughty")
local wibox     = require("wibox")

local battery_widget = wibox.widget.textbox()

function batteryInfo(adapter)
    local raw_current = io.open("/sys/class/power_supply/"..adapter.."/charge_now")
    local current = raw_current:read()
    raw_current:close()

    local raw_capacity = io.open("/sys/class/power_supply/"..adapter.."/charge_full")
    local capacity = raw_capacity:read()
    raw_capacity:close()

    local raw_status = io.open("/sys/class/power_supply/"..adapter.."/status")
    local status = raw_status:read()
    raw_status:close()

    local level = math.floor(current * 100 / capacity)

    local description = ""
    if status:match("Charging") then
        description = "Charging: " .. level .. "%"
    elseif status:match("Discharging") then
        --TODO - I'd like to have time remaining here and above.
        description = "Discharging: " .. level .. "%"

        if tonumber(level) <= 10 then
            naughty.notify({ title      = "Battery Warning"
                           , text       = "Battery low!  " .. level .. "%"
                           , timeout    = 10
                           , position   = "top_right"
                           , fg         = beautiful.fg_focus
                           , bg         = beautiful.bg_focus
                           })
        end
    else
        description = "Charged"
    end
    battery_widget:set_markup(description)

    -- Reset timeout.
    battery_timer.timeout = 30
end

-- Set initial timeout short so that we get information soon after Awesome
-- starts up.
battery_timer = timer({timeout = 1})
battery_timer:connect_signal("timeout", function()
    batteryInfo("BAT0")
end)
battery_timer:start()

return battery_widget
