-- From https://wiki.archlinux.org/index.php/awesome#Setting_up_your_wallpaper

local gears = require("gears")

-- Scan directory, and optionally filter outputs
function scandir(directory, filter)
    local i, t, popen = 0, {}, io.popen
    if not filter then
        filter = function(s) return true end
    end
    print(filter)
    for filename in popen('ls -a "'..directory..'"'):lines() do
        if filter(filename) then
            i = i + 1
            t[i] = filename
        end
    end
    return t
end

local background_path = "/home/hamacher/Pictures/wallpapers/"
local background_filter = function(s) return string.match(s,"%.png$") or string.match(s,"%.jpg$") end
local background_files = scandir(background_path, background_filter)

local background_index = math.random( 1, #background_files)
local background_timeout = 60 * 60  -- In seconds, so one hour.

-- Set up the timer.  We set a short timeout here so that the background is
-- switched quickly from the default when Awesome is first started.  We reset
-- the timeout to the desired one-hour timeout below at line 46.
local background_timer = timer { timeout = 1 }
background_timer:connect_signal("timeout", function()

  -- set wallpaper to current index for all screens
  for s = 1, screen.count() do
    gears.wallpaper.maximized(background_path .. background_files[background_index], s, true)
  end

  -- stop the timer (we don't need multiple instances running at the same time)
  background_timer:stop()

  -- get next random index
  background_index = math.random( 1, #background_files)

  --restart the timer
  background_timer.timeout = background_timeout
  background_timer:start()
end)

-- initial start when rc.lua is first run
background_timer:start()
