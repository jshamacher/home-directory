#!/usr/bin/ruby

require 'fileutils'
require 'yaml'

SOURCE_DIR = '/net/krista-pc/Movies'

$config = YAML.load_file('/home/hamacher/.config/hamacher/cks.yaml')

# Expects a filename and a hash corresponding to the format described in the
# config file.
#
# Returns a hash with the following keys:
# :title -> the title of the movie
# :extension -> the type of the file
# :year -> the year of the movie
# :ignore -> should the movie be ignored
def get_movie_data(filename, data)
    md = filename.match(/^(.+?)(\s+\(\s*(\d{4})\s*\))?\.([^.\s]+)$/)

    result = {}
    result[:title] = data['title'] || md[1]
    result[:extension] = md[4]
    result[:year] = md[3] || data['year'] || 1900
    result[:ignore] = data['ignore'] || false
    result
end

def process_movie(filename)
    # Mark as encountered.
    # TODO - not convinced modifying the config data is a good idea.
    # or just remove it, that might be slightly better.
    $config[:movies][filename]['found'] = true

    movie_data = get_movie_data(filename, $config[:movies][filename])

    # Are we ignoring it?
    if movie_data[:ignore]
        puts "[INFO] Ignoring #{filename} because it is flagged as ignored."
        return
    end

    source = "#{SOURCE_DIR}/#{filename}"
    base_dest = "/media/Movies/#{movie_data[:title]}"
    destination = "#{base_dest} (#{movie_data[:year]}).#{movie_data[:extension]}"

    if File.exist?(destination)
        # Check the size.
        if File.size?(source) != File.size?(destination)
            puts "[ERROR] Size mismatch on #{filename}, not going to copy anything."
        else
            puts "[INFO] Nothing to do for #{filename}"
        end
    elsif !Dir.glob("#{base_dest} (*).*").empty?
        puts "[WARN] Found near matches for #{filename}, not going to copy anything."
    else
        puts "[INFO] Copying #{source} to #{destination}."
        FileUtils.cp source, destination
    end
end

def is_tv_show?(filename)
    filename.match(/^(.*)\s(\d+-)?\d+\.\S+$/) do |md|
        return $config[:tv].has_key?(md[1])
    end
    false
end

# Expects a filename.
#
# Returns a hash with the following keys:
# :title -> the title of the TV show
# :episode -> the year of the TV show
# :season -> the season of the TV show
# :extension -> the type of the file
# :year -> the year of the TV show, or nil if it's not required
# :ignore -> should the TV show be ignored
def get_tv_data(filename)
    md = filename.match(/^(.*)\s((\d+)-)?(\d+)\.(\S+)$/)

    config_key = md[1]

    result = {}
    result[:title] = $config[:tv][config_key]['title'] || md[1]
    result[:year] = $config[:tv][config_key]['year']
    result[:ignore] = $config[:tv][config_key]['ignore'] || false
    result[:season] = md[3] || "1"
    result[:episode] = md[4]
    result[:extension] = md[5]
    result
end

def process_tv_show(filename)

    tv_data = get_tv_data(filename)

    # Are we ignoring it?
    if tv_data[:ignore]
        puts "[INFO] Ignoring #{filename} because it is flagged as ignored."
        return
    end

    source = "#{SOURCE_DIR}/#{filename}"
    dest_dir = "/media/TV Shows/#{tv_data[:title]}" +
               (tv_data[:year].nil? ? "" : " (#{tv_data[:year]})") +
               "/Season #{tv_data[:season]}"
    destination = "#{dest_dir}/" +
                  "s%02de%02d.#{tv_data[:extension]}" %
                  [tv_data[:season], tv_data[:episode]]

    if File.exist?(destination)
        # Check the size.
        if File.size?(source) != File.size?(destination)
            puts "[ERROR] Size mismatch on #{filename}, not going to copy anything."
        else
            puts "[INFO] Nothing to do for #{filename}"
        end
    else
        puts "[INFO] Copying #{source} to #{destination}."
        FileUtils.mkdir_p dest_dir
        FileUtils.cp source, destination
    end
end

def main
    unknowns = []

    Dir.foreach(SOURCE_DIR) do |filename|
        # Besides catching '.' and '..', this has the added bonus of skipping files
        # that are in the process of being copied by rsync.
        next if filename.start_with? '.'

        if $config[:movies].has_key?(filename)
            process_movie filename
        elsif is_tv_show?(filename)
            process_tv_show filename
        elsif $config[:ignored].has_key?(filename)
            puts "[INFO] Ignoring #{filename} because it's on the ignore list."
        else
            # Add to our list of new stuff.
            unknowns << filename
        end
    end

    #TODO - can probably do something fancy with grep or something.
    $config[:movies].each do |movie|
        #TODO - why am I getting a two-element array here?
        puts "[WARN] Did not find #{movie[0]}" unless movie[1]['found']
    end

    unknowns.each do |unknown|
        puts "[ERROR] Found #{unknown}, didn't know what to do with it."
    end
end

main
exit 0
