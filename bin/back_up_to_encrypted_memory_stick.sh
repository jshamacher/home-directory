#!/bin/sh

rsync -avh --delete \
    --exclude .cache \
    --exclude Downloads \
    --exclude .git \
    --exclude Music \
    --exclude Steam \
    --exclude tmp \
    --exclude Trash \
    --exclude VirtualBox\ VMs \
    ~/ /media/hamacher/vault/backup