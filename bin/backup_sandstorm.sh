#!/bin/sh

mkdir -p /home/hamacher/tmp/logs
sudo systemctl stop sandstorm.service
tar cvzf $1/sandstorm-backup-$2.tgz /opt/sandstorm > /home/hamacher/tmp/logs/backup_sandstorm.log
sudo systemctl start sandstorm.service
