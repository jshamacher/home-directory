#!/bin/sh

rm -rf /tmp/trac-backup
mkdir /tmp/trac-backup

cd /opt/Trac
for dir in */; do
    trac-admin $dir hotcopy /tmp/trac-backup/$dir
done

tar cvzf /mnt/backups/trac-backup-`date +%Y-%m-%d`.tgz /tmp/trac-backup

rm -rf /tmp/trac-backup
mkdir /tmp/trac-backup
