#!/usr/bin/env ruby

require 'date'
require 'yaml'

# Look for config file.  Exit if there isn't one.
abort "No config file specified" unless ARGV.length > 0
config_file = ARGV[0]
abort "Unable to open config file #{config_file}" unless File.exist?(config_file)

# Load config file.
config = YAML::load_file(config_file)

# Validate.
abort "No destination defined in #{config_file}" unless config.has_key?('destination')
abort "Destination #{config['destination']} does not exist" unless File.exist?(config['destination'])
abort "Destination #{config['destination']} is not a directory" unless File.directory?(config['destination'])
abort "No scripts defined in #{config_file}" unless config.has_key?('scripts')

now = Date.today()
timestamp = now.strftime('%Y-%m-%d')

config['scripts'].each do |script|
  STDOUT.puts "==========>>> #{script}"

  unless File.executable?(script)
    STDERR.puts "File #{script} does not exist or is not executable, skipping."
    next
  end

  unless(system("#{script} #{config['destination']} #{timestamp}"))
    STDERR.puts "Encountered error running #{script}: #{$?}"
  end
end

files_to_delete = []
total_files = 0

# Clean up destination directory.  We want to keep:
# - The last 7 days of daily backups.
# - The daily backup from the last day of previous six months.
# For example, if today is December 30, we'd keep:
# - June 30, July 31, August 31, September 30, October 31, November 30
# - December 24, 25, 26, 27, 28, 29, 30
# On December 31, we'd delete the December 24 backup.
# On January 01, we'd delete the June 30 backup and the December 25 backup.
Dir.new(config['destination']).each do |filename|
  next if filename.start_with?('.')

  total_files += 1

  # Attempt to extract the timestamp portion of the filename.
  if (/(\d{4})-(\d{2})-(\d{2})/ =~ filename) then
    filedate = Date.new($1.to_i, $2.to_i, $3.to_i)
    if ((now - 7) >= filedate) then
      last_day_of_month = Date.new($1.to_i, $2.to_i, -1)
      if ($3.to_i == last_day_of_month.day) then
        if ((((now.month + 12) - last_day_of_month.month) % 12) > 6) then
          files_to_delete << filename
        end
      else
        files_to_delete << filename
      end
    end
  else
    STDERR.puts "#{filename} doesn't have a timestamp, skipping"
  end
end

failsafe = (files_to_delete.length > (total_files * 0.7))
if (failsafe) then
  STDERR.puts "Found too many files to delete - please verify and delete manually:  #{files_to_delete.join(", ")}"
else
  files_to_delete.each do |filename|
    STDERR.puts "Deleting #{config['destination']}/#{filename}"
    File.delete "#{config['destination']}/#{filename}"
  end
end
