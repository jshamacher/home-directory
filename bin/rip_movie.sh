#!/bin/sh

TITLE_NO=1
YEAR=1900
TITLE=
DISK_NAME=
if [ $# -lt 3 ]; then
    echo "Usage:  $0 [title to rip=1] year disk_name title"
    exit 1
elif [ $# -eq 3 ]; then
    YEAR=$1
    DISK_NAME=$2
    TITLE="$3"
else
    TITLE_NO=$1
    YEAR=$2
    DISK_NAME=$3
    TITLE="$4"
fi

DISK_PATH="/media/hamacher/$DISK_NAME"

HandBrakeCLI \
    -i "$DISK_PATH/VIDEO_TS" \
    -o "/storage/dvd_rip_staging/$TITLE ($YEAR).mp4" \
    --preset="Super HQ 1080p30 Surround" \
    --subtitle scan \
    --subtitle-forced \
    -t "$TITLE_NO" &&
eject "$DISK_PATH"
