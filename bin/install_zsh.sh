#!/bin/sh

cd ~

git clone --recursive https://github.com/jshamacher/prezto .zprezto

ln -s ~/.zprezto/runcoms/zlogin .zlogin
ln -s ~/.zprezto/runcoms/zlogout .zlogout
ln -s ~/.zprezto/runcoms/zpreztorc .zpreztorc
ln -s ~/.zprezto/runcoms/zprofile .zprofile
ln -s ~/.zprezto/runcoms/zshenv .zshenv
ln -s ~/.zprezto/runcoms/zshrc .zshrc

#TODO: Could probably also run chsh, maybe some other stuff?
