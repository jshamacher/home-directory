#!/bin/sh

sudo -H -i -u postgres pg_dumpall | zip > $1/postgres-backup-$2.zip
