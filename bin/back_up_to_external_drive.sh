#!/bin/zsh

SOURCE="/storage/backup/"
DESTINATION=`ls -d /media/hamacher/HamacherBackup[12]/backup 2>/dev/null`

if [ -z "$DESTINATION" ]; then
    echo "Destination directory does not exist!"
    exit 1
fi

if [ ! -w $DESTINATION ]; then
    echo "$DESTINATION is not writable!"
    exit 1
fi

tar cvz "$SOURCE" | gpg2 \
    --batch \
    --no-tty \
    --yes \
    --passphrase-file /home/hamacher/.config/hamacher/backup/passphrase \
    --symmetric \
    --cipher-algo=AES256 \
    --output "$DESTINATION/backup-`date +%Y_%m_%d`.tgz.gpg"

# To decrypt:
# gpg2 --batch --no-tty --yes --passphrase-file ~/.backup_passphrase [filename]
