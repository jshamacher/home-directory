#!/bin/bash

# Configure script over next few lines.
store=/home/hamacher/tmp/var/disk_space_monitoring
threshold=80
for vol in / /home /media; do
    dir=${store}${vol}
    file=$dir/df_output
    if [ -r $file ]; then
        previous=`cat $file`
    else
        previous=0
    fi
    current=$(df $vol | grep $vol | awk '{ print $5}' | sed 's/%//g')
    if [ "$current" -gt "$threshold" ]; then
        if [ "$previous" -le "$threshold" ]; then
            mail -s 'Disk Space Alert' dvr.alerts@brutallogic.net << EOM
Volume '$vol' is now $current% full.
EOM
        fi
    fi
    mkdir -p $dir
    echo -n $current > $file
done
