#!/bin/zsh

#TODO: This should be 'dvr', but DNS is somehow broken for only that host.
MEDIA_SERVER="192.168.1.29"
DESTINATION=`ls -d /media/hamacher/HamacherBackup[12]/media 2>/dev/null`

#if [ ! -r $SOURCE ]; then
#    echo "$SOURCE is not readable!"
#    exit 1
#fi

#TODO: Verify connectivity to media server?

if [ -z "$DESTINATION" ]; then
    echo "$DESTINATION does not exist!"
    exit 1
fi

if [ ! -w $DESTINATION ]; then
    echo "$DESTINATION is not writable!"
    exit 1
fi

# Wallpapers.
#TODO: Consider other pictures?  Would probably want them zipped and encrypted.
rsync -av $MEDIA_SERVER:/media/Pictures/wallpapers $DESTINATION/pictures

# Manuals.
git archive --format=zip --remote=ssh://$MEDIA_SERVER/media/GitRepos/Manuals master > $DESTINATION/manuals.zip

# Music.
#rsync -avn --exclude="*.jpg" --exclude="*.ini" --exclude="Thumbs.db" --delete-excluded $MEDIA_SERVER:/media/Music/ $DESTINATION/music/Collection
