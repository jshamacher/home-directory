# Similar to .zlogin, except sourced before .zshrc.  In general, favor .zlogin
# over this file unless you have a reason not to.

#
# Indeed-specific stuff.
#
if [ -e "$HOME/env/etc/indeed_profile" ]; then
    source "$HOME/env/etc/indeed_profile"
fi

# OPTIONAL, but recommended: Add ~/env/bin to your PATH to use the shared shell scripts from delivery/env
if [ -d "$HOME/env/bin" ]; then
    PATH="$HOME/env/bin:$PATH"
fi

# On OSX, explicitly source .bashrc so that OS X bash is guaranteed to include all definitions
#if [ "Darwin" = "$(uname -s)" ]; then
#  [ -s "$HOME/.bashrc" ] && . "$HOME/.bashrc"
#fi
